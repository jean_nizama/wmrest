﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;
using System.Xml.Serialization;
using System.Xml;
using System.Threading;
using Newtonsoft.Json;
using WMRest.Entities;
using WMRest.Api;
using WMRest.Exceptions;
using SBHTTPSClient;
using SBCertValidator;
using SBX509;

namespace WMRest.Util
{
    public class RestUtil
    {
        private static string Method = "";
        public static string Authorization = "";
        public static string ErrorMessage = "";
        public static string Content = "";
        private static TElHTTPSClient client = null;

        public static List<T> GetList<T>(string uri)
        {
            RestUtil.ErrorMessage = "";
            return Get<List<T>>(uri);
        }

        public static T Get<T>(string uri)
        {
            RestUtil.ErrorMessage = "";
            Method = "GET";
            return JsonToObject<T>(GetRawResponse(Constants.baseUrl+Constants.appUrl+ "/" + uri));
        }

        public static List<T> PostList<T>(string uri,string content)
        {
            RestUtil.ErrorMessage = "";
            return Post<List<T>>(uri,content);
        }

        public static T Post<T>(string uri,string content)
        {
            RestUtil.ErrorMessage = "";
            RestUtil.Content = content;
            Method = "POST";
            return JsonToObject<T>(GetRawResponse(Constants.baseUrl + Constants.appUrl + "/" + uri));
        }

        public static string GetRawResponse(string uri)
        {
            string result = null;
            result = doRequest(uri);
            return result;
        }

        private static void client_OnCertificateValidate(object Sender, TElX509Certificate X509Certificate, ref TSBCertificateValidity Validity, ref int Reason)
        {
            Validity = TSBCertificateValidity.cvOk;
        }
        
        public static string doRequest(string uri)
        {
            string data = null;
            //Stream responseStream = new FileStream("response", FileMode.Create);
            Stream responseStream=new System.IO.MemoryStream();
            client = new TElHTTPSClient();
            client.OnCertificateValidate += new TSBCertificateValidateEvent(client_OnCertificateValidate);
            client.HTTPVersion = SBHTTPSConstants.TSBHTTPVersion.hvHTTP11;
            client.KeepAlivePolicy = SBHTTPSClient.TSBHTTPKeepAlivePolicy.kapStandardDefined;
            client.UseHTTPProxy = false;
            client.UseSocks = false;
            client.UseWebTunneling = false;
            client.CertStorage = null;
            client.SSLEnabled = true;
            client.Versions = SBSSLConstants.Unit.sbTLS12 | SBSSLConstants.Unit.sbTLS11 | SBSSLConstants.Unit.sbTLS1;
            client.OutputStream = responseStream;
            client.RequestParameters.Authorization = RestUtil.Authorization;
            client.RequestParameters.ContentType = "application/json";
            try
            {
                int codeResponse=0;
                if (RestUtil.Method.Equals("GET"))
                {
                    codeResponse = client.Get(uri);
                }
                else 
                {
                    codeResponse = client.Post(uri,RestUtil.Content);
                }

                if (codeResponse != 200)
                {
                    System.Collections.ArrayList headers = client.ResponseHeaders;
                    string httpError = headers[0].ToString();
                    string error = "";
                    string errorDescription = "";
                    for (var i = 0; i < headers.Count; i++)
                    {
                        if (headers[i] != null)
                        {
                            int position = headers[i].ToString().ToLower().IndexOf("www-authenticate:");
                            if (position == 0)
                            {
                                string hs = headers[i].ToString().Substring(17, headers[i].ToString().Length-17);
                                string[] hsArray = hs.Split(',');
                                foreach (string hss in hsArray)
                                {
                                    string[] values = hss.Split('=');
                                    if (values[0].ToLower().Trim().Equals("error"))
                                    {
                                        error = values[1].Replace("\"",String.Empty)+". ";
                                    }
                                    else if (values[0].ToLower().Trim().Equals("error_description"))
                                    {
                                        errorDescription = values[1].Replace("\"",String.Empty)+". ";
                                    }
                                }
                                break;
                            }
                        }
                    }
                    if (error.Trim().Equals("invalid_token."))
                    {
                        throw new InvalidTokenException(error);
                    }
                    else 
                    {
                        throw new Exception(httpError + error + errorDescription);
                    }
                }
                byte[] bytes = new byte[responseStream.Length];
                responseStream.Position = 0;
                responseStream.Read(bytes, 0, (int)responseStream.Length);
                data = System.Text.Encoding.ASCII.GetString(bytes, 0, (int)responseStream.Length);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally 
            {
                client.Close(true);
                if (responseStream != null)
                    responseStream.Close();
                client.OutputStream = null;
            }
            return data;
        }

        public static T JsonToObject<T>(string XMLString)
        {
            return (T) JsonConvert.DeserializeObject<T>(XMLString);
        }
    }
}
