﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace WMRest.Entities
{
    public class Location
    {
        public string oid;
        public string description;
        public string id;
        public string remarks;
        public string locationTypeOid;
        private int level;
        private string warehouseOid;
        private string layoutOid;
        private long revision;

        public Location()
        {
            this.oid = "";
            this.description = "";
            this.id = "";
            this.remarks = "";
            this.locationTypeOid = "";
            this.level = 0;
            this.warehouseOid = "";
            this.layoutOid = "";
            this.revision = 0;
        }

        public Location(string oid, string description, string id, string remarks, string locationTypeOid, int level,string warehouseOid,string layoutOid,long revision)
        {
            this.oid = oid;
            this.description = description;
            this.id = id;
            this.remarks = remarks;
            this.locationTypeOid = locationTypeOid;
            this.level = level;
            this.warehouseOid = warehouseOid;
            this.layoutOid = layoutOid;
            this.revision = revision;
        }

        public override string ToString()
        {
            return String.Format("{0}",id);
        }

    }
}
