﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace WMRest.Entities
{
    public class PnoStatusTask
    {
        public string partId;
        public string status;
        public List<TaskAssetParam> taskAssetParams;

        public PnoStatusTask()
        {
            this.partId = null;
            this.status = null;
            this.taskAssetParams = null;
        }

        public PnoStatusTask(string partId,string status,List<TaskAssetParam> taskAssetParams) 
        { 
            this.partId=partId;
            this.status=status;
            this.taskAssetParams=taskAssetParams;
        }
    }
}
