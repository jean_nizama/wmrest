﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace WMRest.Entities
{
    public class Layout
    {
        public string oid;
        public string drawing;
        public string id;
        public string label;
        public string make;
        public string pattern;
        public string revision;
        public string type;
        public string sequence;
        public string remarks;

        public Layout()
        {
            this.oid ="";
            this.drawing = "";
            this.id = "";
            this.label = "";
            this.make = "";
            this.pattern = "";
            this.revision = "";
            this.type = "";
            this.sequence = "";
            this.remarks = "";
        }

        public Layout(string oid,string drawing,string id,string label,string make,string pattern,string revision,string type,string sequence,string remarks) 
        {
            this.oid = oid;
            this.drawing = drawing;
            this.id = id;
            this.label = label;
            this.make = make;
            this.pattern = pattern;
            this.revision = revision;
            this.type = type;
            this.sequence = sequence;
            this.remarks = remarks;
        }
    }
}
