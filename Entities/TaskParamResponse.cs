﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace WMRest.Entities
{
    public class TaskParamResponse
    {
        public long id;
        public bool allSerialExisting;
        public long revision;
        public long scanRevision;
        public List<String> nonExistingSerials;
        public List<TaskSerial> nonExistingSerialAndPartIds;

        public TaskParamResponse()
        {
            this.id = 0;
            this.allSerialExisting = false;
            this.revision = 0;
            this.scanRevision = 0;
            this.nonExistingSerials = null;
            this.nonExistingSerialAndPartIds = null;
        }

        public TaskParamResponse(long id,bool allSerialExisting,long revision,long scanRevision,List<String> nonExistingSerials, List<TaskSerial> nonExistingSerialAndPartIds) 
        { 
            this.id=id;
            this.allSerialExisting=allSerialExisting;
            this.revision=revision;
            this.scanRevision=scanRevision;
            this.nonExistingSerials = nonExistingSerials;
            this.nonExistingSerialAndPartIds = nonExistingSerialAndPartIds;
        }

        public override string ToString()
        {
            return String.Format("id:{0}, revision:{1}, scanRevision:{2}", id,revision,scanRevision);
        }
    }
}
