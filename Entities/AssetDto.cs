﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace WMRest.Entities
{
    public class AssetDto
    {
        public string oid;
        public string containerSerialNumber;
        public string containerPartId;
        public string serialNumber;
        public string tagId;
        public string alternateSerial;
        public int vendorId;
        public string mfgDate;
        public string expDate;
        public string lastScanDateTime;
        public string batch;
        public string statusOid;
        public string assetCategoryOid;
        public string ownerOid;
        public string remarks;
        public string field1;
        public string field2;
        public string note1;
        public string note2;
        public string tid;
        public string partId;
        public string categoryOid;
        public string name;
        public string modelOid;
        public int level;
	    public int version;
        public List<ExtendedField> extendedFields;

        public AssetDto()
        {
            this.oid = null;
            this.containerSerialNumber = null;
            this.containerPartId = null;
            this.serialNumber = null;
            this.tagId = null;
            this.alternateSerial = null;
            this.vendorId = 0;
            this.mfgDate = null;
            this.expDate = null;
            this.lastScanDateTime = null;
            this.batch = null;
            this.statusOid = null;
            this.assetCategoryOid = null;
            this.ownerOid = null;
            this.remarks = null;
            this.field1 = null;
            this.field2 = null;
            this.note1 = null;
            this.note2 = null;
            this.tid = null;
            this.partId = null;
            this.categoryOid = null;
            this.name = null;
            this.modelOid = null;
            this.level =0;
            this.version = 0;
            this.extendedFields = null;
        }

        public AssetDto(string oid,string containerSerialNumber,string containerPartId,string serialNumber,string tagId,string alternateSerial,int vendorId,string mfgDate,string expDate,string lastScanDateTime,string batch,string statusOid,string assetCategoryOid,string ownerOid,string remarks,string field1,string field2,string note1,string note2,string tid,string partId,string categoryOid,string name,string modelOid,int level,int version,List<ExtendedField> extendedFields) 
        { 
            this.oid=oid;
            this.containerSerialNumber=containerSerialNumber;
            this.containerPartId=containerPartId;
            this.serialNumber=serialNumber;
            this.tagId=tagId;
            this.alternateSerial=alternateSerial;
            this.vendorId=vendorId;
            this.mfgDate=mfgDate;
            this.expDate=expDate;
            this.lastScanDateTime=lastScanDateTime;
            this.batch=batch;
            this.statusOid=statusOid;
            this.assetCategoryOid=assetCategoryOid;
            this.ownerOid=ownerOid;
            this.remarks=remarks;
            this.field1=field1;
            this.field2=field2;
            this.note1=note1;
            this.note2=note2;
            this.tid=tid;
            this.partId=partId;
            this.categoryOid=categoryOid;
            this.name=name;
            this.modelOid=modelOid;
            this.level=level;
	        this.version=version;
            this.extendedFields=extendedFields;
        }
    }
}
