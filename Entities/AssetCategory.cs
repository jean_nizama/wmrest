﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace WMRest.Entities
{
    public class AssetCategory
    {
        private string oid;
        private string description;
        private string id;
        private string remarks;

        public AssetCategory()
        {
            this.oid = "";
            this.description = "";
            this.id = "";
            this.remarks = "";
        }

        public AssetCategory(string oid,string description,string id,string remarks) 
        {
            this.oid = oid;
            this.description = description;
            this.id = id;
            this.remarks = remarks;
        }
    }
}
