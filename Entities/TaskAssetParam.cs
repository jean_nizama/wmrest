﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace WMRest.Entities
{
    public class TaskAssetParam
    {
        public string serialNumber;
        public string assetCategoryId;
        public string note1;
        public string note2;

        public TaskAssetParam()
        {
            this.serialNumber = null;
            this.assetCategoryId = null;
            this.note1 = null;
            this.note2 = null;
        }

        public TaskAssetParam(string serialNumber,string assetCategoryId,string note1,string note2)
        {
            this.serialNumber=serialNumber;
            this.assetCategoryId=assetCategoryId;
            this.note1=note1;
            this.note2=note2;
        }
    }
}
