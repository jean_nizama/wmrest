﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WMRest.Entities
{
    public class Token
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public string refresh_token { get; set; }
        public long expires_in { get; set; }
        public string scope { get; set; }

        public Token()
        {
            this.access_token = "";
            this.token_type = "";
            this.refresh_token = "";
            this.expires_in = 0;
            this.scope = "";
        }

        public Token(string access_token, string token_type, string refresh_token, long expires_in, string scope)
        {
            this.access_token = access_token;
            this.token_type = token_type;
            this.refresh_token = refresh_token;
            this.expires_in = expires_in;
            this.scope = scope;
        }

        public override string ToString()
        {
            return String.Format("acces_token: {0} token_type: {1} refresh_token: {2} expires_in: {3} scope: {4}", access_token, token_type, refresh_token, expires_in, scope);
        }
    }
}
