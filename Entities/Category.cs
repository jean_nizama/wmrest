﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace WMRest.Entities
{
    public class Category
    {
        public string oid;
        public string description;
        public string remarks;

        public Category()
        {
            this.oid = "";
            this.description = "";
            this.remarks = "";
        }

        public Category(string oid,string description,string remarks) 
        {
            this.oid = oid;
            this.description = description;
            this.remarks = remarks;
        }
    }
}
