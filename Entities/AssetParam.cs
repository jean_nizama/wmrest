﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace WMRest.Entities
{
    public class AssetParam
    {
        public string warehouseOid;
        public string locationOid;
        public List<PnoStatus> pnoStatus;

        public AssetParam()
        {
            this.warehouseOid = "";
            this.locationOid = "";
            this.pnoStatus = null;
        }

        public AssetParam(string warehouseOid,string locationOid,List<PnoStatus> pnoStatus) 
        { 
            this.warehouseOid=warehouseOid;
            this.locationOid=locationOid;
            this.pnoStatus = pnoStatus;
        }
    }
}
