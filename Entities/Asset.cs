﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace WMRest.Entities
{
    public class Asset
    {
        public string oid;
        public string serialNumber;
        public string alternateSerial;
        public string name;
        public string tagId;
        public string batch;
        public string mfgDate;
        public string expDate;
        public string statusOid;
        public string partId;
        public string lastScanDateTime;
        public string field1;
        public string field2;
        public string special;
        public string tid;
        public bool generateTagId;
        public string remarks;
        public string warehouseOid;
        public string locationOid;
        public string modelOid;
        public string assetCategoryOid;
        public string ownerOid;
        public int vendorId;
        public string containerAssetOid;
        public string superContainerAssetOid;
        public int level;
        public int version;
        public string encondingId;
        public string unitOid;
        public string categoryOid;
        public List<ExtendedField> extendedFields;

        public Asset()
        {
            this.oid = "";
            this.serialNumber = "";
            this.alternateSerial = "";
            this.name = "";
            this.tagId = "";
            this.batch = "";
            this.mfgDate = "";
            this.expDate = "";
            this.statusOid = "";
            this.partId = "";
            this.lastScanDateTime = "";
            this.field1 = "";
            this.field2 = "";
            this.special = "";
            this.tid = "";
            this.generateTagId = false;
            this.remarks = "";
            this.warehouseOid = "";
            this.locationOid = "";
            this.modelOid = "";
            this.assetCategoryOid = "";
            this.ownerOid = "";
            this.vendorId = 0;
            this.containerAssetOid = "";
            this.superContainerAssetOid = "";
            this.level = 0;
            this.version = 0;
            this.encondingId = "";
            this.unitOid = "";
            this.categoryOid = "";
            this.extendedFields = new List<ExtendedField>();
        }

        public Asset(string oid,string serialNumber,string alternateSerial,string name,string tagId,string batch,string mfgDate,string expDate,string statusOid,string partId,string lastScanDateTime,string field1,string field2,string special,string tid,bool generateTagId,string remarks,string warehouseOid,string locationOid,string modelOid,string assetCategoryOid,string ownerOid,int vendorId,string containerAssetOid,string superContainerAssetOid,int level,int version,string encondingId,string unitOid,string categoryOid, List<ExtendedField> extendedFields) 
        {
            this.oid = oid;
            this.serialNumber = serialNumber;
            this.alternateSerial = alternateSerial;
            this.name = name;
            this.tagId = tagId;
            this.batch = batch;
            this.mfgDate = mfgDate;
            this.expDate = expDate;
            this.statusOid = statusOid;
            this.partId = partId;
            this.lastScanDateTime = lastScanDateTime;
            this.field1 = field1;
            this.field2 = field2;
            this.special = special;
            this.tid = tid;
            this.generateTagId = generateTagId;
            this.remarks = remarks;
            this.warehouseOid = warehouseOid;
            this.locationOid = locationOid;
            this.modelOid = modelOid;
            this.assetCategoryOid = assetCategoryOid;
            this.ownerOid = ownerOid;
            this.vendorId = vendorId;
            this.containerAssetOid = containerAssetOid;
            this.superContainerAssetOid = superContainerAssetOid;
            this.level = level;
            this.version = version;
            this.encondingId = encondingId;
            this.unitOid = unitOid;
            this.categoryOid = categoryOid;
            this.extendedFields = extendedFields;
        }
    }
}
