﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace WMRest.Entities
{
    public class Warehouse
    {
        public string oid;
        public string description;
        public string id;
        public string label;
        public string remarks;
        public string status;

        public Warehouse()
        {
            this.oid = "";
            this.description = "";
            this.id = "";
            this.label = "";
            this.remarks = "";
            this.status = "";
        }

        public Warehouse(string oid, string description, string id, string label, string remarks, string status)
        {
            this.oid = oid;
            this.description = description;
            this.id = id;
            this.label = label;
            this.remarks = remarks;
            this.status = status;
        }

        public override string ToString()
        {
            return String.Format("oid:{0}, description:{1}, id:{2}, label:{3}, remarks: {4}, status:{5} ", oid, description,id,label,remarks,status);
        }
    }
}
