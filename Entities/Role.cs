﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace WMRest.Entities
{
    public class Role
    {
        public string role;
        public List<DeviceType> deviceType;
        public List<Category> categories;
        public List<Operation> operations;

        public Role()
        {
            this.role = "";
            this.deviceType = new List<DeviceType>();
            this.categories = new List<Category>();
            this.operations = new List<Operation>();
        }

        public Role(string role,List<DeviceType> deviceType,List<Category> categories,List<Operation> operations) 
        {
            this.role = role;
            this.deviceType = deviceType;
            this.categories = categories;
            this.operations = operations;
        }

    }

    public class Operation 
    {
        public string name;
        public string remarks;

        public Operation()
        {
            this.name = "";
            this.remarks = "";
        }

        public Operation(string name,string remarks) 
        {
            this.name = name;
            this.remarks = remarks;
        }
    }

    public class DeviceType 
    { 
        public string name;
        public string remarks;

        public DeviceType()
        {
            this.name = "";
            this.remarks = "";
        }

        public DeviceType(string name, string remarks) 
        {
            this.name = name;
            this.remarks = remarks;
        }
    }
}
