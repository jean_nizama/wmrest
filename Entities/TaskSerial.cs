﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace WMRest.Entities
{
    public class TaskSerial
    {
        public string locationTaskId;
        public string serialNumber;
        public string partId;
        public string assetCategoryId;
        public string field1;
        public string field2;
        public string status;

        public TaskSerial()
        {
            this.locationTaskId = null;
            this.serialNumber = null;
            this.partId = null;
            this.assetCategoryId = null;
            this.field1 = null;
            this.field2 = null;
            this.status = null;
        }

        public TaskSerial(string locationTaskId,string serialNumber,string partId,string assetCategoryId,string field1,string field2,string status) 
        { 
            this.locationTaskId=locationTaskId;
            this.serialNumber=serialNumber;
            this.partId=partId;
            this.assetCategoryId=assetCategoryId;
            this.field1=field1;
            this.field2=field2;
            this.status=status;        
        }
    }
}
