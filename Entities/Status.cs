﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WMRest.Entities
{
    public class Status
    {
        public string oid;
        public string description;
        public string remarks;

        public Status() 
        {
            this.oid = "";
            this.description = "";
            this.remarks = "";
        }

        public Status(string oid, string description, string remarks)
        {
            this.oid = oid;
            this.description = description;
            this.remarks = remarks;
        }

        public override string ToString()
        {
            return String.Format("oid: {0}, description: {1}, remarks: {2} ", oid, description, remarks);
        }
    }
}
