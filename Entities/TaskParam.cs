﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace WMRest.Entities
{
    public class TaskParam
    {
        public string locationOid;
        public string locationId;
        public string layoutId;
        public string scanDateTime;
        public string taskType;
        public string scanType;
        public string username;
        public string device;
        public string deviceSerialNumber;
        public string geolocation;
        public string remarks;
        public string flightNumber;
        public string workCard;
        public string deviceVersion;
        public string approvedBy;
        public string count;
        public List<AssetParam> assetParams;
        public List<PnoStatusTask> taskParams;
        public List<TaskAssetParam> unknownAssets;


        public TaskParam()
        {
            this.locationOid = null;
            this.locationId = null;
            this.layoutId = null;
            this.scanDateTime = null;
            this.taskType = null;
            this.scanType = null;
            this.username = null;
            this.device = null;
            this.deviceSerialNumber = null;
            this.geolocation = null;
            this.remarks = null;
            this.flightNumber = null;
            this.workCard = null;
            this.deviceVersion = null;
            this.approvedBy = null ;
            this.count = null;
            this.assetParams = null;
            this.taskParams = null;
            this.unknownAssets = null;
        }

        public TaskParam(string locationOid,string locationId,string layoutId,string scanDateTime,string taskType,string scanType,string username,string device,string deviceSerialNumber,string geolocation,string remarks,string flightNumber,string workCard,string deviceVersion,string approvedBy,string count,List<AssetParam> assetParams,List<PnoStatusTask> taskParams,List<TaskAssetParam> unknownAssets) 
        { 
            this.locationOid=locationOid;
            this.locationId=locationId;
            this.layoutId = layoutId;
            this.scanDateTime=scanDateTime;
            this.taskType=taskType;
            this.scanType=scanType;
            this.username=username;
            this.device=device;
            this.deviceSerialNumber=deviceSerialNumber;
            this.geolocation=geolocation;
            this.remarks=remarks;
            this.flightNumber=flightNumber;
            this.workCard=workCard;
            this.deviceVersion=deviceVersion;
            this.approvedBy=approvedBy;
            this.count=count;
            this.assetParams=assetParams;
            this.taskParams=taskParams;
            this.unknownAssets=unknownAssets;
        }
    }
}
