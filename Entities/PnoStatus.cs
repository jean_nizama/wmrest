﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace WMRest.Entities
{
    public class PnoStatus
    {
        public string partId;
        public string statusOid;
        public List<AssetDto> assets;

        public PnoStatus()
        {
            this.partId =null;
            this.statusOid = null;
            this.assets = null;
        }

        public PnoStatus(string partId,string statusOid,List<AssetDto> assets) 
        {
            this.partId = partId;
            this.statusOid = statusOid;
            this.assets = assets;
        }
    }
}
