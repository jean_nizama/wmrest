﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace WMRest.Entities
{
    public class Vendor
    {
        public int id;
        public string name;
        public string cageCode;
        public string lastSerialNumber;
        public string remarks;
        public int version;

        public Vendor()
        {
            this.id = 0;
            this.name = "";
            this.cageCode = "";
            this.lastSerialNumber = "";
            this.remarks = "";
            this.version = 0;
        }

        public Vendor(int id,string name,string cageCode,string lastSerialNumber,string remarks,int version) 
        { 
            this.id = id;
            this.name = name;
            this.cageCode=cageCode;
            this.lastSerialNumber=lastSerialNumber;
            this.remarks=remarks;
            this.version = version;
        }
    }
}
