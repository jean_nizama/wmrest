﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace WMRest.Entities
{
    public class ExtendedField
    {
        public string name;
        public string value;
        public string type;

        public ExtendedField() 
        { 
            this.name="";
            this.value = "";
            this.type = "";
        }

        public ExtendedField(string name, string value, string type)
        {
            this.name = name;
            this.value = value;
            this.type = type;
        }
    }
}
