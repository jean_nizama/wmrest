﻿namespace WMRest
{
    partial class Home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.locationCmbBox = new System.Windows.Forms.ComboBox();
            this.operationCmbBox = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(15, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(210, 23);
            this.label1.Text = "Location:";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(15, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(209, 26);
            this.label2.Text = "Operation:";
            // 
            // locationCmbBox
            // 
            this.locationCmbBox.Location = new System.Drawing.Point(15, 38);
            this.locationCmbBox.Name = "locationCmbBox";
            this.locationCmbBox.Size = new System.Drawing.Size(208, 22);
            this.locationCmbBox.TabIndex = 2;
            // 
            // operationCmbBox
            // 
            this.operationCmbBox.Items.Add("getAssetsByLocationOidOptimize");
            this.operationCmbBox.Items.Add("getWarehouses");
            this.operationCmbBox.Items.Add("getVendors");
            this.operationCmbBox.Items.Add("getAssetCategories");
            this.operationCmbBox.Items.Add("getLayouts");
            this.operationCmbBox.Items.Add("getStatus");
            this.operationCmbBox.Items.Add("getCategories");
            this.operationCmbBox.Items.Add("getRolesByUserName");
            this.operationCmbBox.Items.Add("getAll");
            this.operationCmbBox.Items.Add("postTask");
            this.operationCmbBox.Location = new System.Drawing.Point(15, 93);
            this.operationCmbBox.Name = "operationCmbBox";
            this.operationCmbBox.Size = new System.Drawing.Size(207, 22);
            this.operationCmbBox.TabIndex = 3;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(28, 142);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(177, 25);
            this.button1.TabIndex = 4;
            this.button1.Text = "Execute";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.operationCmbBox);
            this.Controls.Add(this.locationCmbBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Menu = this.mainMenu1;
            this.Name = "Home";
            this.Text = "Home";
            this.Load += new System.EventHandler(this.Home_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox locationCmbBox;
        private System.Windows.Forms.ComboBox operationCmbBox;
        private System.Windows.Forms.Button button1;

    }
}