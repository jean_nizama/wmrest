﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WMRest.Api;
using WMRest.Entities;
using WMRest.Util;
using Newtonsoft.Json;
using System.Security.Cryptography;
using SBLDAPSCore;
using SBLDAPSClient;
using SBLDAPCertStorage;
using SBX509;

namespace WMRest
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
            authenticationCmb.SelectedIndex = 0;
            SBUtils.Unit.SetLicenseKey("5CFA21A9AD04F5B5A2F0E6077875986733F142102F6077D39CD07AB0A7B994A49BFDF98A9C45DE669FA8C400EB6DF7733D0A02BF091E5C14AE92DA6FE25F2BB9E31EA93BE215A0EE717FF1D9E75CE408183B78715D15EEE726CE596348AFE5E713B03677ED46F01F91EFAEA9CD7E0CB1491F3F9DD711B28E6B6FCE422648AACD62E66E6B9C0F675BBA022C4A6B6B37595B901B36EE59C2171C8C38C9E1F73B9921A12FB0270E88FE19992FBCB0AB51B5E810397FF6E383586CB39ED9D63FC1897D9EBB8982162E5885DB993B772F2D352519C702CBC59E9DADC08A2E6DDC07E9E94A7CFDB193D2D420AE7FAC3F9DBD483327DCE4BDCC721B14BF17D05B20F7F8");
        }

        private string decodePwd(string toDecode)
        {
            SHA1 sha1 = SHA1CryptoServiceProvider.Create();
            byte[] hash = sha1.ComputeHash(Encoding.ASCII.GetBytes(toDecode));
            string pwd = Convert.ToBase64String(hash);
            return pwd;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (authenticationCmb.SelectedIndex == 1)
                {
                    TElLDAPSClient LDAPSClient = new TElLDAPSClient();
                    LDAPSClient.OnCertificateValidate += new TSBCertificateValidateEvent(DoCertificateValidate);
                    LDAPSClient.AuthType = TSBLDAPAuthenticationType.autSimple;
                    LDAPSClient.Address = "192.168.30.15";
                    LDAPSClient.Port = 389;
                    //LDAPSClient.Port = 636;
                    LDAPSClient.LDAPDN = userNameTxt.Text + "@eamrfid.miami";
                    LDAPSClient.Password = passwordTxt.Text;
                    LDAPSClient.Bind();
                }

                Constants.baseUrl = serverTxt.Text;
                Constants.userName = userNameTxt.Text;
                Constants.password = decodePwd(passwordTxt.Text);
                ClientRest clientRest = ClientRest.Instance;
                clientRest.getToken();
                Home h = new Home();
                h.Show();
            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.Message,"Error");
            }
        }

        private void DoCertificateValidate(object Sender, TElX509Certificate X509Certificate, ref TSBCertificateValidity Validity, ref int Reason)
        {
            Validity = TSBCertificateValidity.cvOk;
            Reason = 0;
        }
    }
}