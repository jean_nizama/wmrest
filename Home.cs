﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WMRest.Api;
using WMRest.Entities;

namespace WMRest
{
    public partial class Home : Form
    {
        public Home()
        {
            InitializeComponent();
        }

        private void Home_Load(object sender, EventArgs e)
        {
            ClientRest clientRest = ClientRest.Instance;
            List<Location> locations = clientRest.locationService.getAll();
            locationCmbBox.DataSource = locations;
            locationCmbBox.DisplayMember = "description";
            locationCmbBox.SelectedValueChanged += new EventHandler(locationCmbBox_SelectedValueChanged);
        }

        void locationCmbBox_SelectedValueChanged(object sender, EventArgs e)
        {
            Location l = locationCmbBox.SelectedItem as Location;
            if (l != null)
            {
                operationCmbBox.Enabled = true;
                button1.Enabled = true;
            }
            else
            {
                operationCmbBox.Enabled = false;
                button1.Enabled = false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ClientRest clientRest = ClientRest.Instance;
            string result = "";
            try
            {
                if (operationCmbBox.SelectedItem.ToString().Equals("getAssetsByLocationOidOptimize"))
                {
                    Location l = locationCmbBox.SelectedItem as Location;
                    List<Asset> assets = clientRest.assetService.getByLocationOid(l.oid);
                    result += "assets:" + assets.Count + "\n";
                }
                else if (operationCmbBox.SelectedItem.ToString().Equals("getWarehouses"))
                {
                    List<Warehouse> warehouses = clientRest.warehouseService.getAll();
                    result += "warehouses:" + warehouses.Count + "\n";
                }
                else if (operationCmbBox.SelectedItem.ToString().Equals("getVendors"))
                {
                    List<Vendor> vendors = clientRest.vendorService.getAll();
                    result += "vendors:" + vendors.Count + "\n";
                }
                else if (operationCmbBox.SelectedItem.ToString().Equals("getAssetCategories"))
                {
                    List<AssetCategory> assetCategories = clientRest.assetCategoryService.getAll();
                    result += "assetCategories:" + assetCategories.Count + "\n";
                }
                else if (operationCmbBox.SelectedItem.ToString().Equals("getLayouts"))
                {
                    List<Layout> layouts = clientRest.layoutService.getAll();
                    result += "layouts:" + layouts.Count + "\n";
                }
                else if (operationCmbBox.SelectedItem.ToString().Equals("getStatus"))
                {
                    List<Status> status = clientRest.statusService.getAll();
                    result += "status:" + status.Count + "\n";
                }
                else if (operationCmbBox.SelectedItem.ToString().Equals("getCategories"))
                {
                    List<Category> categories = clientRest.categoryService.getAll();
                    result += "categories:" + categories.Count + "\n";
                }
                else if (operationCmbBox.SelectedItem.ToString().Equals("getRolesByUserName"))
                {
                    List<Role> roles = clientRest.roleService.getByUser(Constants.userName);
                    result += "roles:" + roles.Count + "\n";
                }
                else if (operationCmbBox.SelectedItem.ToString().Equals("postTask"))
                {                    
                    AssetDto assetDto = new AssetDto();
                    assetDto.serialNumber = "NRA000849";
                    assetDto.tagId = "3B3A0D76DD6E76DB6C31B78C7000E481C30C38D39000";
                    assetDto.mfgDate = "2012-12-12T00:00:00.000+0000";
                    assetDto.expDate = "2022-12-12T00:00:00.000+0000";
                    assetDto.tid = "";
                    assetDto.assetCategoryOid = "";
                    assetDto.vendorId=61;

                    List<AssetDto> assets = new List<AssetDto>();
                    assets.Add(assetDto);

                    PnoStatus pno=new PnoStatus();
                    pno.partId = "66601-810";
                    pno.statusOid = "2c9f9a2043ef44c90143fcbc71000026";
                    pno.assets = assets;

                    List<PnoStatus> pnoStatus = new List<PnoStatus>();
                    pnoStatus.Add(pno);

                    AssetParam assetParam = new AssetParam();
                    assetParam.warehouseOid = "2c9f93f6545402260154718ab80c000a";
                    assetParam.locationOid = "c617ba985a63da2b015a862b92d002a0";
                    assetParam.pnoStatus = pnoStatus;

                    List<AssetParam> assetParams=new List<AssetParam>();
                    assetParams.Add(assetParam);

                    TaskParam taskParam = new TaskParam();
                    taskParam.locationId = "J001";
                    taskParam.locationOid = "c617ba985a63da2b015a862b92d002a0";
                    taskParam.layoutId = "CAB1X5";
                    taskParam.scanDateTime = "2017-02-28T15:18:15-05:00";
                    taskParam.taskType = "Assign";
                    taskParam.scanType = "Date Expiry";
                    taskParam.username = "jnizama";
                    taskParam.device = "TCMobile";
                    taskParam.deviceSerialNumber = "0000003313009003";
                    taskParam.geolocation = "T7";
                    taskParam.remarks = "";
                    taskParam.flightNumber = "";
                    taskParam.workCard = "";
                    taskParam.deviceVersion = "2.0.12.2";
                    taskParam.approvedBy = "";
                    taskParam.count = "";
                    taskParam.assetParams = assetParams;

                    TaskParamResponse response = clientRest.taskService.postTask(taskParam);
                    result += "TaskParamReponse: " +response;
                }
                else
                {
                    List<Status> status = clientRest.statusService.getAll();
                    result += "Status:" + status.Count + "\n";
                    List<Category> categories = clientRest.categoryService.getAll();
                    result += "Categories:" + categories.Count + "\n";
                    List<Layout> layouts = clientRest.layoutService.getAll();
                    result += "Layouts:" + layouts.Count + "\n";
                    List<Location> locations = clientRest.locationService.getAll();
                    result += "Locations:" + locations.Count + "\n";
                    List<Vendor> vendors = clientRest.vendorService.getAll();
                    result += "Vendors:" + vendors.Count + "\n";
                    List<Warehouse> warehouses = clientRest.warehouseService.getAll();
                    result += "warehouses:" + warehouses.Count + "\n";
                    Location l = locationCmbBox.SelectedItem as Location;
                    List<Asset> assets = clientRest.assetService.getByLocationOid(l.oid);
                    result += "assets:" + assets.Count + "\n";
                    List<AssetCategory> assetCategories = clientRest.assetCategoryService.getAll();
                    result += "assetCategories:" + assetCategories.Count + "\n";
                    List<Role> roles = clientRest.roleService.getByUser(Constants.userName);
                    result += "roles:" + roles.Count + "\n";
                }
                MessageBox.Show(result);
            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.Message);
            }
        }
    }
}