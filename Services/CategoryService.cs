﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using WMRest.Entities;
using WMRest.Util;

namespace WMRest.Services
{
    public class CategoryService:Service
    {
        public CategoryService(Token token) 
        {
            this.token = token;
        }

        public List<Category> getAll()
        {
            List<Category> list = null;
            RestUtil.Authorization = "Bearer " + this.token.access_token;
            list = RestUtil.GetList<Category>("api/category");
            return list;
        }

        public Category getByOid(string oid)
        {
            Category entity = null;
            RestUtil.Authorization = "Bearer " + this.token.access_token;
            entity = RestUtil.Get<Category>("api/category/" + oid);
            return entity;
        }
    }
}
