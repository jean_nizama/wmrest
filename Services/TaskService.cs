﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using WMRest.Entities;
using WMRest.Util;
using Newtonsoft.Json;

namespace WMRest.Services
{
    public class TaskService:Service
    {
        public TaskService(Token token) 
        {
            this.token = token;
        }


        public TaskParamResponse postTask(TaskParam taskParam)
        {
            TaskParamResponse response= null;
            RestUtil.Authorization = "Bearer " + this.token.access_token;
            string content=JsonConvert.SerializeObject(taskParam);
            response = RestUtil.Post<TaskParamResponse>("api/assets",content);
            return response;
        }
    }
}
