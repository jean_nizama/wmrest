﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using WMRest.Entities;
using WMRest.Util;

namespace WMRest.Services
{
    public class StatusService:Service
    {
        public StatusService(Token token) 
        {
            this.token = token;
        }

        public List<Status> getAll()
        {
            List<Status> list = null;
            RestUtil.Authorization = "Bearer " + this.token.access_token;
            list = RestUtil.GetList<Status>("api/status");
            return list;
        }

        public Status getByOid(string oid)
        {
            Status entity = null;
            RestUtil.Authorization = "Bearer " + this.token.access_token;
            entity = RestUtil.Get<Status>("api/status/" + oid);
            return entity;
        }
    }
}
