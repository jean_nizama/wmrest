﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using WMRest.Entities;
using WMRest.Util;

namespace WMRest.Services
{
    public class VendorService:Service
    {
        public VendorService(Token token) 
        {
            this.token = token;
        }

        public List<Vendor> getAll()
        {
            List<Vendor> list = null;
            RestUtil.Authorization = "Bearer " + this.token.access_token;
            list = RestUtil.GetList<Vendor>("api/vendor");
            return list;
        }

        public Vendor getById(string id)
        {
            Vendor entity = null;
            RestUtil.Authorization = "Bearer " + this.token.access_token;
            entity = RestUtil.Get<Vendor>("api/vendor/" + id);
            return entity;
        }
    }
}
