﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using WMRest.Entities;
using WMRest.Util;

namespace WMRest.Services
{
    public class LayoutService:Service
    {
        public LayoutService(Token token) 
        {
            this.token = token;
        }

        public List<Layout> getAll()
        {
            List<Layout> list = null;
            RestUtil.Authorization = "Bearer " + this.token.access_token;
            list = RestUtil.GetList<Layout>("api/layout");
            return list;
        }

        public Layout getByOid(string oid)
        {
            Layout entity = null;
            RestUtil.Authorization = "Bearer " + this.token.access_token;
            entity = RestUtil.Get<Layout>("api/layout/" + oid);
            return entity;
        }
    }
}
