﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using WMRest.Entities;
using WMRest.Util;
using WMRest.Exceptions;
using WMRest.Api;

namespace WMRest.Services
{
    public class AssetService:Service
    {
        public AssetService(Token token) 
        {
            this.token = token;
        }

        public List<Asset> getByLocationOid(String locationOid)
        {
            List<Asset> list = null;
            RestUtil.Authorization = "Bearer " + this.token.access_token;
            list = RestUtil.GetList<Asset>("api/asset/location/" + locationOid);
            return list;
        }

    }
}
