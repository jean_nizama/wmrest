﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using WMRest.Entities;
using WMRest.Util;

namespace WMRest.Services
{
    public class TokenService
    {
        private string authUsername;
        private string authPassword;
        private string userName;
        private string password;

        public TokenService(string authUsername,string authPassword,string userName, string password)
        {
            this.authUsername = authUsername;
            this.authPassword = password;
            this.userName = userName;
            this.password = password;
        }

        public Token getToken()
        {
            Token token = null;
            try
            {
                RestUtil.Authorization = "Basic " + Convert.ToBase64String(Encoding.ASCII.GetBytes("mobile:secret"));
                token = RestUtil.Post<Token>("oauth/token?grant_type=password&username=" + this.userName + "&password=" + this.password,"");
                Console.Write(token);
                Console.Write("Exito");
            }
            catch (Exception e) 
            {
                string m = e.Message;
                throw new Exception(m);
            }
            return token;
        }
    }
}
