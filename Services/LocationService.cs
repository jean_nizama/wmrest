﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using WMRest.Entities;
using WMRest.Util;

namespace WMRest.Services
{
    public class LocationService:Service
    {
        public LocationService(Token token) 
        {
            this.token = token;
        }

        public List<Location> getAll()
        {
            List<Location> listStatus = null;
            RestUtil.Authorization = "Bearer " + this.token.access_token;
            listStatus = RestUtil.GetList<Location>("api/location");
            return listStatus;
        }

        public Location getByOid(string oid)
        {
            Location entity = null;
            RestUtil.Authorization = "Bearer " + this.token.access_token;
            entity = RestUtil.Get<Location>("api/location/" + oid);
            return entity;
        }
    }
}
