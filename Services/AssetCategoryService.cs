﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using WMRest.Entities;
using WMRest.Util;

namespace WMRest.Services
{
    public class AssetCategoryService:Service
    {
        public AssetCategoryService(Token token) 
        {
            this.token = token;
        }

        public List<AssetCategory> getAll()
        {
            List<AssetCategory> list = null;
            RestUtil.Authorization = "Bearer " + this.token.access_token;
            list = RestUtil.GetList<AssetCategory>("api/assetcategory");
            return list;
        }

        public AssetCategory getByOid(string oid)
        {
            AssetCategory entity = null;
            RestUtil.Authorization = "Bearer " + this.token.access_token;
            entity = RestUtil.Get<AssetCategory>("api/assetcategory/" + oid);
            return entity;
        }
    }
}
