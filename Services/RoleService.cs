﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using WMRest.Entities;
using WMRest.Util;

namespace WMRest.Services
{
    public class RoleService:Service
    {
        public RoleService(Token token) 
        {
            this.token = token;
        }

        public List<Role> getByUser(string user)
        {
            List<Role> list = null;
            RestUtil.Authorization = "Bearer " + this.token.access_token;
            list = RestUtil.GetList<Role>("api/user/roles/"+user);
            return list;
        }
    }
}
