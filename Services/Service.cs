﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using WMRest.Entities;

namespace WMRest.Services
{
    public class Service
    {
        public Token token { get; set; }

        public Service() {}

        public Service(Token token) 
        {
            this.token = token;
        }
    }
}
