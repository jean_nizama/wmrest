﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using WMRest.Entities;
using WMRest.Util;

namespace WMRest.Services
{
    public class WarehouseService:Service
    {
        public WarehouseService(Token token) 
        {
            this.token = token;
        }

        public List<Warehouse> getAll()
        {
            List<Warehouse> list = null;
            RestUtil.Authorization = "Bearer " + this.token.access_token;
            list = RestUtil.GetList<Warehouse>("api/warehouse");
            return list;
        }

        public Warehouse getByOid(string oid)
        {
            Warehouse entity = null;
            RestUtil.Authorization = "Bearer " + this.token.access_token;
            entity = RestUtil.Get<Warehouse>("api/warehouse/" + oid);
            return entity;
        }
    }
}
