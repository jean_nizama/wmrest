﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using WMRest.Services;
using WMRest.Entities;
using WMRest.Util;
using System.Net;

namespace WMRest.Api
{
    public class ClientRest
    {
        private static ClientRest instance;
        private Token token;
        public TokenService tokenService;
        public StatusService statusService;
        public CategoryService categoryService;
        public LayoutService layoutService;
        public LocationService locationService;
        public VendorService vendorService;
        public WarehouseService warehouseService;
        public AssetService assetService;
        public AssetCategoryService assetCategoryService;
        public RoleService roleService;
        public TaskService taskService;

        private ClientRest() 
        {
            //SBUtils.Unit.SetLicenseKey("5CFA21A9AD04F5B5A2F0E6077875986733F142102F6077D39CD07AB0A7B994A49BFDF98A9C45DE669FA8C400EB6DF7733D0A02BF091E5C14AE92DA6FE25F2BB9E31EA93BE215A0EE717FF1D9E75CE408183B78715D15EEE726CE596348AFE5E713B03677ED46F01F91EFAEA9CD7E0CB1491F3F9DD711B28E6B6FCE422648AACD62E66E6B9C0F675BBA022C4A6B6B37595B901B36EE59C2171C8C38C9E1F73B9921A12FB0270E88FE19992FBCB0AB51B5E810397FF6E383586CB39ED9D63FC1897D9EBB8982162E5885DB993B772F2D352519C702CBC59E9DADC08A2E6DDC07E9E94A7CFDB193D2D420AE7FAC3F9DBD483327DCE4BDCC721B14BF17D05B20F7F8");
        }

        public static ClientRest Instance 
        {
            get 
            {
                if (instance == null)
                {
                    instance = new ClientRest();
                }
                return instance;
            }
        }

        public void getToken() 
        {
            try
            {                
                TokenService tokenRepository = new TokenService(Constants.authUsername, Constants.authPassword, Constants.userName, Constants.password);
                this.token = tokenRepository.getToken();
                statusService = new StatusService(this.token);
                categoryService=new CategoryService(this.token);
                layoutService = new LayoutService(this.token);
                locationService = new LocationService(this.token);
                vendorService = new VendorService(this.token);
                warehouseService = new WarehouseService(this.token);
                assetService = new AssetService(this.token);
                assetCategoryService = new AssetCategoryService(this.token);
                roleService = new RoleService(this.token);
                taskService = new TaskService(this.token);
            }
            catch(Exception ex)
            {
                throw new Exception("Error getting Token.- "+ex.Message);
            }
        }
    }
}