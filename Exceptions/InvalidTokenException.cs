﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace WMRest.Exceptions
{
    public class InvalidTokenException:Exception
    {
        public InvalidTokenException()
        {
        }

        public InvalidTokenException(string message): base(message)
        {
        }

        public InvalidTokenException(string message, Exception inner): base(message, inner)
        {
        }
    }
}
